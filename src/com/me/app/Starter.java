package com.me.app;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.me.logger.BinaryLogger;
import com.me.logger.CompressedLogger;
import com.me.logger.Logger;
import com.me.logger.SerializedLogger;
import com.me.logger.Student;
import com.me.logger.StudentParser;
import com.me.logger.TextLogger;

public class Starter {

	private TextLogger textLog;
	private CompressedLogger compressedLog;
	private BinaryLogger binaryLog;
	private SerializedLogger serializedLog;
	private Set<Logger> loggers = new HashSet<Logger>();

	public Starter() throws IOException {
		textLog = new TextLogger("Asd.txt");
		compressedLog = new CompressedLogger("bsc.zip");
		binaryLog = new BinaryLogger("bin.bin");
		serializedLog = new SerializedLogger("ser.bin");
		loggers.add(textLog);
		loggers.add(compressedLog);
		loggers.add(binaryLog);
		loggers.add(serializedLog);
	}

	public void start() throws IOException {
		List<Student> students = StudentParser.parse(new File("students.txt"));

		for (Student st : students) {
			for (Logger logger : loggers) {
				logger.log("added", st);
			}
		}
	}
}
