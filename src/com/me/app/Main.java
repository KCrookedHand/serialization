package com.me.app;

import java.io.IOException;

public class Main {
	
	public static void main(String[] args) throws IOException {
		Starter starter = new Starter();
		starter.start();
	}

}
